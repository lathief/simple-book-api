package com.example.bookapi.controller;

import com.example.bookapi.exception.BookNotFoundException;
import com.example.bookapi.model.Book;
import com.example.bookapi.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class BookController {
    @Autowired
    BookRepository bookRepository;

    @GetMapping("/books")
    public ResponseEntity<?> getAllBooks(){
        List<Book> books = bookRepository.findAll();
        return ResponseEntity.ok(books);
    }
    @GetMapping("/books/{id}")
    public ResponseEntity<?> getBooksById(@PathVariable(value = "id") Long BookId) throws BookNotFoundException {
        Book book = bookRepository.findById(BookId).orElseThrow(() -> new BookNotFoundException(BookId));
        return ResponseEntity.ok(book);
    }

    @PostMapping("/books")
    public ResponseEntity<?> createBook(@Valid @RequestBody Book book){
        bookRepository.save(book);
        return ResponseEntity.ok("Book Created");
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<?> editBook(@PathVariable(value = "id") Long BookId, @Valid @RequestBody Book book) throws BookNotFoundException {
        Book bookFound = bookRepository.findById(BookId).orElseThrow(() -> new BookNotFoundException(BookId));

        bookFound.setBook_name(book.getBook_name());
        bookFound.setAuthor_name(book.getAuthor_name());
        bookFound.setIsbn(book.getIsbn());

        Book updatedBook = bookRepository.save(bookFound);
        return ResponseEntity.ok("Book edited");
    }

    @DeleteMapping("/books/{id}")
    public ResponseEntity<?> deleteBooksById(@PathVariable(value = "id") Long BookId) throws BookNotFoundException {
        Book book = bookRepository.findById(BookId).orElseThrow(() -> new BookNotFoundException(BookId));
        bookRepository.delete(book);
        return ResponseEntity.ok("book deleted");
    }
}
